// Copyright (C) 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glBeginQuery.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glBeginQuery.xhtml","OpenGL ES 3.1")
cmd void glBeginQuery(GLenum target, QueryId query) {
  minRequiredVersion(3, 0)
  switch (target) {
    case GL_ANY_SAMPLES_PASSED, GL_ANY_SAMPLES_PASSED_CONSERVATIVE,
        GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }

}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glDeleteQueries.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glDeleteQueries.xhtml","OpenGL ES 3.1")
cmd void glDeleteQueries(GLsizei count, const QueryId* queries) {
  minRequiredVersion(3, 0)

  q := queries[0:count]
  ctx := GetContext()
  for i in (0 .. count) {
    ctx.Instances.Queries[q[i]] = null
  }
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glBeginQuery.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glBeginQuery.xhtml","OpenGL ES 3.1")
cmd void glEndQuery(GLenum target) {
  minRequiredVersion(3, 0)
  switch (target) {
    case GL_ANY_SAMPLES_PASSED, GL_ANY_SAMPLES_PASSED_CONSERVATIVE,
        GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }

}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glGenQueries.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glGenQueries.xhtml","OpenGL ES 3.1")
cmd void glGenQueries(GLsizei count, QueryId* queries) {
  minRequiredVersion(3, 0)

  q := queries[0:count]
  ctx := GetContext()
  for i in (0 .. count) {
    id := as!QueryId(?)
    ctx.Instances.Queries[id] = new!Query()
    q[i] = id
  }
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glGetQueryObjectuiv.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glGetQueryObjectuiv.xhtml","OpenGL ES 3.1")
cmd void glGetQueryObjectuiv(QueryId query, GLenum parameter, GLuint* value) {
  minRequiredVersion(3, 0)
  switch (parameter) {
    case GL_QUERY_RESULT, GL_QUERY_RESULT_AVAILABLE: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(parameter)
    }
  }

  value[0] = ?
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glGetQueryiv.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glGetQueryiv.xhtml","OpenGL ES 3.1")
cmd void glGetQueryiv(GLenum target, GLenum parameter, GLint* value) {
  minRequiredVersion(3, 0)
  switch (target) {
    case GL_ANY_SAMPLES_PASSED, GL_ANY_SAMPLES_PASSED_CONSERVATIVE,
        GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }
  switch (parameter) {
    case GL_CURRENT_QUERY: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(parameter)
    }
  }

  value[0] = ?
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glIsQuery.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glIsQuery.xhtml","OpenGL ES 3.1")
cmd bool glIsQuery(QueryId query) {
  minRequiredVersion(3, 0)

  ctx := GetContext()
  return query in ctx.Instances.Queries
}

