{{/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */}}

{{/* ---- Includes ---- */}}
{{Include "cpp_common.tmpl"}}

{{$filename := print (Global "API") "_imports.cpp" }}
{{$ | Macro "imports.cpp" | Format (Global "clang-format") | Write $filename}}

{{/*
-------------------------------------------------------------------------------
  Entry point.
-------------------------------------------------------------------------------
*/}}
{{define "imports.cpp"}}
{{template "C++.AOSP.Copyright"}}
¶
#include "{{Global "API"}}_imports.h"
¶
#include <gapic/get_gfx_proc_address.h>
¶
namespace gapii {
¶
  void {{Title (Global "API")}}Imports::Resolve() {
    using namespace gapic;
    {{range $c := AllCommands $}}
      {{if not (GetAnnotation $c "synthetic")}}
        {{$name := Macro "CmdName" $c}}
        {{$name}} = reinterpret_cast<{{Template "FunctionPtrType" $c}}>(GetGfxProcAddress("{{$name}}", true));
      {{end}}
    {{end}}
  }
¶
} // namespace gapii
¶
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the typedef function-pointer name for a given command.
-------------------------------------------------------------------------------
*/}}
{{define "FunctionPtrType"}}
  {{AssertType $ "Function"}}

  PFN{{Upper (Macro "CmdName" $)}}
{{end}}
