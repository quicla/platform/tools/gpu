// Copyright (C) 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package opcode

import (
	"io"

	"android.googlesource.com/platform/tools/gpu/binary/endian"
	"android.googlesource.com/platform/tools/gpu/binary/flat"
)

// Disassemble disassembles and returns the stream of encoded Opcodes from r,
// stopping once an EOF is reached.
func Disassemble(r io.Reader, byteOrder endian.ByteOrder) ([]interface{}, error) {
	d := flat.Decoder(endian.Reader(r, byteOrder))
	opcodes := []interface{}{}
	for {
		opcode, err := Decode(d)
		switch err {
		case nil:
			opcodes = append(opcodes, opcode)
		case io.EOF:
			return opcodes, nil
		default:
			return nil, err
		}
	}
}
