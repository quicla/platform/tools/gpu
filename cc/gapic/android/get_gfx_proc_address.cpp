/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../dl_loader.h"
#include "../log.h"

#include <string>
#include <unordered_map>

#if defined(__LP64__)
#define SYSTEM_LIB_PATH "/system/lib64/"
#else
#define SYSTEM_LIB_PATH "/system/lib/"
#endif

namespace gapic {

namespace {

void* ResolveSymbol(const char* name, bool bypassLocal) {
    typedef void* (*GPAPROC)(const char *name);

    if (bypassLocal) {
        static DlLoader libegl(SYSTEM_LIB_PATH "libEGL.so");
        if (GPAPROC gpa = reinterpret_cast<GPAPROC>(libegl.lookup("eglGetProcAddress"))) {
            if (void* proc = gpa(name)) {
                GAPID_INFO("GetGfxProcAddress(%s, %d) -> 0x%x (via libEGL eglGetProcAddress)\n", name, bypassLocal, proc);
                return proc;
            }
        }
        if (void* proc = libegl.lookup(name)) {
            GAPID_INFO("GetGfxProcAddress(%s, %d) -> 0x%x (from libEGL dlsym)\n", name, bypassLocal, proc);
            return proc;
        }

        static DlLoader libglesv2(SYSTEM_LIB_PATH "libGLESv2.so");
        if (void* proc = libglesv2.lookup(name)) {
            GAPID_INFO("GetGfxProcAddress(%s, %d) -> 0x%x (from libGLESv2 dlsym)\n", name, bypassLocal, proc);
            return proc;
        }

        static DlLoader libglesv1(SYSTEM_LIB_PATH "libGLESv1_CM.so");
        if (void* proc = libglesv1.lookup(name)) {
            GAPID_INFO("GetGfxProcAddress(%s, %d) -> 0x%x (from libGLESv1_CM dlsym)\n", name, bypassLocal, proc);
            return proc;
        }
    } else {
        static DlLoader local(nullptr);
        if (GPAPROC gpa = reinterpret_cast<GPAPROC>(local.lookup("eglGetProcAddress"))) {
            if (void* proc = gpa(name)) {
                GAPID_INFO("GetGfxProcAddress(%s, %d) -> 0x%x (via local eglGetProcAddress)\n", name, bypassLocal, proc);
                return proc;
            }
        }
        if (void* proc = local.lookup(name)) {
            GAPID_INFO("GetGfxProcAddress(%s, %d) -> 0x%x (from local dlsym)\n", name, bypassLocal, proc);
            return proc;
        }
    }

    GAPID_INFO("GetGfxProcAddress(%s, %d) -> not found\n", name, bypassLocal);
    return nullptr;
}

} // anonymous namespace

void* GetGfxProcAddress(const char* name, bool bypassLocal) {
    GAPID_INFO("GetGfxProcAddress(%s, %d)", name, bypassLocal);

    static std::unordered_map<std::string, void*> cache;

    const std::string cacheKey = std::string(name) + (bypassLocal ? "/direct" : "/local");
    auto it = cache.find(cacheKey);
    if (it != cache.end()) {
        GAPID_INFO("GetGfxProcAddress(%s, %d) -> 0x%x (from cache)\n", name, bypassLocal, it->second);
        return it->second;
    }

    void* proc = ResolveSymbol(name, bypassLocal);
    cache[cacheKey] = proc;
    return proc;
}

}  // namespace gapic
